/* vi:set et ai sw=4 sts=4 ts=4: */
/*-
 * Copyright (c) 2017 Frederik Möllers <frederik@die-sinlosen.de>
 * Copyright (c) 2024 Stephen Robinson <stephen@drsudo.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
#include <gio/gio.h>
#include <stdlib.h>

#include "debug.h"
#include "nextcloud-socket.h"

#define NC_CLIENT_SOCKET_RELPATH "/Nextcloud/socket"

#define NC_CMD_VERSION "VERSION:\n"
#define NC_CMD_GET_STRINGS "GET_STRINGS:\n"
#define NC_CMD_GET_MENU_ITEMS "GET_MENU_ITEMS:%s\n"
#define NC_CMD_RETRIEVE_FILE_STATUS "RETRIEVE_FILE_STATUS:%s\n"
#define NC_CMD_RETRIEVE_FOLDER_STATUS "RETRIEVE_FOLDER_STATUS:%s\n"

static struct timeval socket_timeout = {.tv_sec = 0, .tv_usec = 500000};

static bool is_shutdown = false;

static int socket_fd = -1;
static GIOChannel * socket_channel = NULL;
static guint socket_event_source_id = 0;
static guint socket_event_source_id_2 = 0;

static char * version = NULL;
static GList * synced_dirs = NULL;
static GQueue * dirs_to_check_status = NULL;
static GHashTable * strings = NULL;

static char * split_on_char(char *, char);
static int handle_response(char * cmd, char * args);

static void disconnect_socket(void)
{
    if (socket_channel != NULL) {
        g_source_remove(socket_event_source_id);
        g_source_remove(socket_event_source_id_2);
        g_io_channel_unref(socket_channel);
        socket_channel = NULL;
    }
    if (socket_fd >= 0) {
        close(socket_fd);
        socket_fd = -1;
    }

    if (synced_dirs != NULL) {
        g_list_free_full(synced_dirs, g_free);
        synced_dirs = NULL;
    }

    if (dirs_to_check_status != NULL) {
        g_queue_free_full(dirs_to_check_status, g_free);
        dirs_to_check_status = NULL;
    }

    if (strings != NULL) {
        g_hash_table_destroy(strings);
        strings = NULL;
    }

    if (version != NULL) {
        g_free(version);
        version = NULL;
    }
}

static bool set_receive_timeout(__suseconds_t usec)
{
    int ret = 0;
    int flags = fcntl(socket_fd, F_GETFL, 0);
    if (usec == 0) {
        flags |= O_NONBLOCK;
    } else {
        struct timeval timeout = {.tv_sec = 0, .tv_usec = usec};
        ret = setsockopt(socket_fd,
                         SOL_SOCKET,
                         SO_RCVTIMEO,
                         (char*) &timeout,
                         sizeof(timeout));
        flags &= ~O_NONBLOCK;
    }
    return ret | fcntl(socket_fd, F_SETFL, flags);
}

static gboolean handle_io_watch_err(GIOChannel * source,
                                    GIOCondition condition,
                                    gpointer data)
{
    if (is_shutdown) {
        return FALSE;
    }
    g_warning("Recieved error GIOCondition: %d. Reconnecting...", condition);
    ncsock_initialize();
    return FALSE;
}

static gboolean handle_io_watch(GIOChannel * source,
                                GIOCondition condition,
                                gpointer data)
{
    if (is_shutdown) {
        return FALSE;
    }
    char * line = NULL;
    gsize term_pos = 0;
    while (g_io_channel_read_line(source, &line, NULL, &term_pos, NULL) == G_IO_STATUS_NORMAL) {
        line[term_pos] = '\0'; // Remove newline char
        char * args = split_on_char(line, ':');
        if (args == NULL) {
            g_warning("Received malformed Nextcloud message line: %s", line);
        } else if (handle_response(line, args) != 0) {
            DEBUG_G_MESSAGE("Received command: %s:%s", line, args);
        }
        g_free(line);
    }
    return TRUE;
}

static gboolean connect_socket(gpointer data)
{
    if (is_shutdown) {
        return FALSE;
    }

    /* Create local socket. */
    socket_fd = socket(AF_UNIX, SOCK_STREAM, 0);
    if (socket_fd < 0)
    {
        g_warning("socket() failed! %s", strerror(errno));
        return TRUE;
    }

    if(setsockopt(socket_fd,
                  SOL_SOCKET,
                  SO_SNDTIMEO,
                  (char*) &socket_timeout,
                  sizeof(socket_timeout)) < 0)
    {
        close(socket_fd);
        g_warning("setsockopt() failed! %s", strerror(errno));
        return TRUE;
    }

    if (set_receive_timeout(0) != 0) {
        close(socket_fd);
        g_warning("set_receive_timeout() failed! %s", strerror(errno));
        return TRUE;
    }

    /*
    * For portability clear the whole structure, since some
    * implementations have additional (nonstandard) fields in
    * the structure.
    */
    struct sockaddr_un addr;
    memset(&addr, 0, sizeof(struct sockaddr_un));

    /* Connect socket to socket address */
    addr.sun_family = AF_UNIX;

    char const* xdg_runtime_dir = g_getenv("XDG_RUNTIME_DIR");
    if (xdg_runtime_dir == NULL || *xdg_runtime_dir == '\0') {
        close(socket_fd);
        g_warning("XDG_RUNTIME_DIR is unset");
        return TRUE;
    }

    snprintf(addr.sun_path, sizeof(addr.sun_path),
             "%s%s", xdg_runtime_dir, NC_CLIENT_SOCKET_RELPATH);

    if (connect(socket_fd, (const struct sockaddr*) &addr, sizeof(struct sockaddr_un)) < 0)
    {
        g_warning("connect() failed! %s", strerror(errno));
        close(socket_fd);
        return TRUE;
    }

    DEBUG_G_MESSAGE("Connected to '%s'", addr.sun_path);

    socket_channel = g_io_channel_unix_new(socket_fd);
    socket_event_source_id = g_io_add_watch(socket_channel,
                                            G_IO_IN,
                                            handle_io_watch,
                                            NULL);

    socket_event_source_id_2 = g_io_add_watch(socket_channel,
                                              G_IO_HUP | G_IO_ERR | G_IO_NVAL,
                                              handle_io_watch_err,
                                              NULL);

    ncsock_send(NC_CMD_VERSION);
    ncsock_send(NC_CMD_GET_STRINGS);

    return FALSE;
}

void ncsock_initialize(void)
{

    // This is here in case we are re-initializing
    disconnect_socket();
    is_shutdown = false;

    dirs_to_check_status = g_queue_new();
    strings = g_hash_table_new_full(g_str_hash, g_str_equal, g_free, g_free);

    if (connect_socket(NULL)) { // NOTE: returns TRUE on error!
        g_timeout_add(5000, connect_socket, NULL);
    }
}

void ncsock_shutdown(void) {
    is_shutdown = true;
    disconnect_socket();
}

bool ncsock_connected(void)
{
    return socket_fd >= 0;
}

int ncsock_send(const char * cmdfmt, ...)
{

    if (socket_channel == NULL) {
        g_warning("Trying to send Nextcloud command to closed socket");
        return -1;
    }

    va_list cmdargs;
    va_start(cmdargs, cmdfmt);

    char* cmd = g_strdup_vprintf(cmdfmt, cmdargs);
    ssize_t len = strlen(cmd);
    ssize_t sent = send(socket_fd, cmd, strlen(cmd), 0);
    if (sent < len) {
        g_warning("Unable to send command! %s\n", strerror(errno));
        ncsock_initialize(); // Reconnect
        return -1;
    }

    g_free(cmd);

    return 0;
}

bool ncsock_is_in_registered_paths(const char * dirpath)
{
    char absdir[PATH_MAX];
    if (realpath(dirpath, absdir) == NULL) {
        return false;
    }

    for (GList* sync_dir = synced_dirs; sync_dir != NULL; sync_dir = sync_dir->next) {
        char abssync[PATH_MAX];
        if (realpath(sync_dir->data, abssync) == NULL) {
            continue;
        }
        if (strncmp(abssync, absdir, strlen(abssync)) == 0) {
            return true;
        }
    }
    return false;
}

static char * split_on_char(char * s, char c)
{
    char * delim = strchr(s, c);
    if (delim == NULL) {
        return NULL;
    }
    *delim = 0;
    return delim + 1;
}

static void check_status(const char * path, gboolean recurse)
{
    if (!g_file_test(path, G_FILE_TEST_IS_DIR)) {
        // Normally check_status is called on directories, but in case it gets
        // called on a file, handle that gracefully.
        ncsock_send(NC_CMD_RETRIEVE_FILE_STATUS, path);
    } else {
        // This command will get the status of all the files in this folder.
        ncsock_send(NC_CMD_RETRIEVE_FOLDER_STATUS, path);

        if (recurse) {
            // We want to recursivly check all the folders under this one. To
            // avoid blocking on large directory trees (or slow filesystems), we
            // will only recurse one level at a time. These subdirs will be added
            // to the queue of diretories to check. One directory will be checked
            // on the next idle task.
            GDir *dir = g_dir_open(path, 0, NULL);
            if (dir == NULL) {
                return;
            }

            const gchar * name;
            while ((name = g_dir_read_name(dir)) != NULL) {
                gchar * subpath = g_build_filename(path, name, NULL);
                // Only add directories. All the files have already been checked
                // via NC_CMD_RETRIEVE_FOLDER_STATUS.
                //
                // Nextcloud does not support symlinks because of the potential
                // for cycles.
                if (g_file_test(subpath, G_FILE_TEST_IS_DIR) &&
                    !g_file_test(subpath, G_FILE_TEST_IS_SYMLINK)
                ) {
                    g_queue_push_tail(dirs_to_check_status, g_strdup(subpath));
                }
                g_free(subpath);
            }

            g_dir_close(dir);
        }
    }
}

static gboolean check_dir_status_callback(gpointer data)
{
    if (is_shutdown) {
        return FALSE;
    }

    // Get a pending path.
    const char * dirpath;
    while ((dirpath = g_queue_pop_head(dirs_to_check_status)) != NULL) {
        // Make sure that this dir_path is a subdirectory of a registered directory.
        if (ncsock_is_in_registered_paths(dirpath)) {
            break;
        }
    }
    if (dirpath == NULL) {
        return FALSE;
    }

    check_status(dirpath, TRUE);
    g_free((gpointer)dirpath);

    return TRUE;
}

static int handle_cmd_string(char * args)
{
    char * value = split_on_char(args, ':');
    if (value == NULL) {
        g_warning("Recieved STRING args with no colon: %s", args);
        return -1;
    }

    DEBUG_G_MESSAGE("Recieved STRING (key: %s, value: %s)", args, value);
    g_hash_table_insert(strings, g_strdup(args), g_strdup(value));

    return 0;
}

static int handle_cmd_version(char * args)
{
    DEBUG_G_MESSAGE("Recieved VERSION: %s", args);
    version = g_strdup(args);
    return 0;
}

static int handle_register_path(char * args)
{
    DEBUG_G_MESSAGE("Registering path: %s", args);
    synced_dirs = g_list_append(synced_dirs, g_strdup(args));
    if (dirs_to_check_status->length != 0) {
        // If the dirs to check queue is empty, then the check status callback
        // will not be called. Start up an idle task to call it.
        g_idle_add(check_dir_status_callback, NULL);
    }
    check_status(args, TRUE);
    return 0;
}

static int handle_unregister_path(char * args)
{
    GList* item = g_list_find_custom(synced_dirs, args, (GCompareFunc)strcmp);
    if (item) {
        synced_dirs = g_list_remove_link(synced_dirs, item);
        g_list_free_full(item, g_free);
        DEBUG_G_MESSAGE("Unregistered directory: %s", args);
    } else {
        g_warning("Failed to unregister directory: %s", args);
    }

    if (synced_dirs == NULL) {
        // Last synced dir was removed. We should try to reconnect.
        ncsock_initialize();
    }

    return 0;
}

static const char * status_to_emblem_name(const char * state)
{
    if (strcmp(state, "OK") == 0) {
        return "Nextcloud_ok";
    }
    if (strcmp(state, "SYNC") == 0) {
        return "Nextcloud_sync";
    }
    if (strcmp(state, "NEW") == 0) {
        return "Nextcloud_sync";
    }
    if (strcmp(state, "IGNORE") == 0) {
        return "Nextcloud_warn";
    }
    if (strcmp(state, "ERROR") == 0) {
        return "Nextcloud_error";
    }
    if (strcmp(state, "OK+SWM") == 0) {
        return "Nextcloud_ok_shared";
    }
    if (strcmp(state, "SYNC+SWM") == 0) {
        return "Nextcloud_sync_shared";
    }
    if (strcmp(state, "NEW+SWM") == 0) {
        return "Nextcloud_sync_shared";
    }
    if (strcmp(state, "IGNORE+SWM") == 0) {
        return "Nextcloud_warn_shared";
    }
    if (strcmp(state, "ERROR+SWM") == 0) {
        return "Nextcloud_error_shared";
    }
    // state == NOP
    return NULL;
}

static int handle_status(char * args)
{
    char * status = args;
    char * filename = split_on_char(args, ':');
    if (filename == NULL) {
        g_warning("Recieved STATUS command with no filename");
        return -1;
    }

    DEBUG_G_MESSAGE("Recieved STATUS (file: %s, status: %s)", filename, status);

    const char * emblem_name = status_to_emblem_name(status);
    gchar ** emblemv = NULL;

    GFile * file = g_file_new_for_path(filename);
    GFileInfo * info = g_file_query_info(file,
                                         "metadata::emblems:",
                                         G_FILE_QUERY_INFO_NONE,
                                         NULL,
                                         NULL);

    if (info == NULL) {
        g_warning("Unable to get file info for STATUS file: %s", filename);
        goto cleanup;
    }

    if (emblem_name == NULL) {
        g_file_info_remove_attribute(info, "metadata::emblems");
    } else {
        emblemv = g_new0 (gchar *, 1);
        emblemv[0] = g_strdup(emblem_name);
        g_file_info_set_attribute_stringv(info, "metadata::emblems", emblemv);
    }

    g_file_set_attributes_async(file, info,
                                G_FILE_QUERY_INFO_NONE,
                                G_PRIORITY_DEFAULT,
                                NULL,
                                NULL,
                                NULL);
cleanup:
    g_object_unref(file);
    if (emblemv != NULL) {
        g_strfreev(emblemv);
    }
    if (info != NULL) {
        g_object_unref(info);
    }

    return 0;
}

static int handle_update_view(char * args)
{
    DEBUG_G_MESSAGE("Recieved UPDATE_VIEW (path: %s)", args);
    check_status(args, FALSE);
    return 0;
}

static int handle_response(char * cmd, char * args)
{
    if (strcmp(cmd, "STRING") == 0) {
        return handle_cmd_string(args);
    } else if (strcmp(cmd, "VERSION") == 0) {
        return handle_cmd_version(args);
    } else if (strcmp(cmd, "REGISTER_PATH") == 0) {
        return handle_register_path(args);
    } else if (strcmp(cmd, "UNREGISTER_PATH") == 0) {
        return handle_unregister_path(args);
    } else if (strcmp(cmd, "STATUS") == 0) {
        return handle_status(args);
    } else if (strcmp(cmd, "UPDATE_VIEW") == 0) {
        return handle_update_view(args);
    } else {
        return -1;
    }
}

static bool handle_menu_item_response(char * cmd, char * args, GList ** items)
{
    if (strcmp(cmd, "MENU_ITEM") == 0) {
        char * action = args;
        char * disable = split_on_char(action, ':');
        if (disable == NULL) {
            g_warning("Got invalid MENU_ITEM");
            return true;
        }
        char * label = split_on_char(disable, ':');
        if (label == NULL) {
            g_warning("Got invalid MENU_ITEM");
            return true;
        }

        struct nc_menu_item * item = g_malloc(sizeof(struct nc_menu_item));
        item->action = g_strdup(action);
        item->disable = disable[0] == 'd';
        item->label = g_strdup(label);

        DEBUG_G_MESSAGE("Got MENU_ITEM \"%s\" (%s, %s)",
                        item->label,
                        item->action,
                        item->disable ? "disabled" : "enabled");

        *items = g_list_append(*items, item);
    } else if (strcmp(cmd, "GET_MENU_ITEMS") == 0) {
        if (strcmp(args, "END") == 0) {
            return false;
        }
    } else {
        handle_response(cmd, args);
    }
    return true;
}

GList * ncsock_get_menu_items_list(const char * filepath)
{
    ncsock_send(NC_CMD_GET_MENU_ITEMS, filepath);

    const int timeout_us = 100 * 1000;
    const gint64 start_us = g_get_monotonic_time();
    set_receive_timeout(timeout_us);

    GList * items = NULL;

    bool list_done = false;
    while(!list_done && (g_get_monotonic_time() - start_us) < timeout_us) {
        char * line = NULL;
        gsize term_pos = 0;
        while (g_io_channel_read_line(socket_channel, &line, NULL, &term_pos, NULL) == G_IO_STATUS_NORMAL) {
            line[term_pos] = '\0'; // Remove newline char
            char * args = split_on_char(line, ':');
            if (args == NULL) {
                g_warning("Received malformed Nextcloud message line: %s", line);
            } else {
                list_done = !handle_menu_item_response(line, args, &items);
            }
            g_free(line);

            if (list_done) {
                set_receive_timeout(0);
            }
        }
    }

    // Make sure that this gets reset properly
    set_receive_timeout(0);

    return items;
}

static void free_menu_item(struct nc_menu_item * item)
{
    g_free(item->action);
    g_free(item->label);
}

void ncsock_free_menu_items_list(GList * list)
{
    g_list_free_full(list, (GDestroyNotify)free_menu_item);
}

const char * ncsock_get_string(const char * name, const char * default_string)
{
    const char * ret = g_hash_table_lookup(strings, name);
    if (ret == NULL) {
        return default_string;
    } else {
        return ret;
    }
}
