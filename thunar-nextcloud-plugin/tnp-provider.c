/* vi:set et ai sw=4 sts=4 ts=4: */
/*-
 * Copyright (c) 2006 Benedikt Meurer <benny@xfce.org>
 * Copyright (c) 2011 Jannis Pohlmann <jannis@xfce.org>
 * Copyright (c) 2017 Frederik Möllers <frederik@die-sinlosen.de>
 * Copyright (c) 2024 Stephen Robinson <stephen@drsudo.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include <unistd.h>
#include <libxfce4util/libxfce4util.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>

#include "nextcloud-socket.h"
#include "tnp-provider.h"

// forward declarations
static void tnp_provider_menu_provider_init (ThunarxMenuProviderIface* iface);
static void tnp_provider_finalize (GObject* object);
static GList* tnp_provider_get_file_menu_items(ThunarxMenuProvider* menu_provider,
                                            GtkWidget* window,
                                            GList* files);

struct _TnpProviderClass
{
    GObjectClass __parent__;
};

struct _TnpProvider
{
    GObject __parent__;

    #if !GTK_CHECK_VERSION(2,9,0)
    /*
    * GTK+ 2.9.0 and above provide an icon-name property
    * for GtkActions, so we don't need the icon factory.
    */
    GtkIconFactory *icon_factory;
    #endif

    // taken from thunar-archive-plugin and kept just to be safe
    gint            child_watch_id;
};

static GQuark tnp_item_files_quark;
static GQuark tnp_item_provider_quark;
static GQuark tnp_item_action_quark;

THUNARX_DEFINE_TYPE_WITH_CODE(TnpProvider,
                              tnp_provider,
                              G_TYPE_OBJECT,
                              THUNARX_IMPLEMENT_INTERFACE (THUNARX_TYPE_MENU_PROVIDER,
                                                           tnp_provider_menu_provider_init));

static void tnp_item_do_action(ThunarxMenuItem *item, GtkWidget* window)
{
    char realpath_buffer[PATH_MAX];

    /* determine the files associated with the action */
    GList * files = g_object_get_qdata (G_OBJECT (item), tnp_item_files_quark);
    if (G_UNLIKELY (files == NULL || files->next != NULL))
    {
        return;
    }

    // get the file's path
    gchar * path = thunarx_file_info_get_uri(files->data);
    gchar * path_unescaped = g_filename_from_uri(path, NULL, NULL);
    g_free(path);
    if(realpath(path_unescaped, realpath_buffer) == NULL)
    {
        g_warning("Failed to resolve path for nextcloud share: '%s'", path_unescaped);
        g_free(path_unescaped);
        return;
    }
    g_free(path_unescaped);
    //g_list_free_full(files, g_free);

    gchar * action = g_object_get_qdata (G_OBJECT (item), tnp_item_action_quark);
    if (G_UNLIKELY (action == NULL))
    {
        return;
    }
    ncsock_send("%s:%s\n", action, realpath_buffer);
}

static bool resolve_path(char * uri, char * path_buffer)
{
    if (G_UNLIKELY(strncmp(uri, "file://", strlen("file://")) != 0)) {
        return false;
    }
    char * unescaped = g_filename_from_uri(uri, NULL, NULL);
    if(realpath(unescaped, path_buffer) == NULL)
    {
        // dereference the path minus the leading "file://"
        if(realpath(uri + 7, path_buffer) == NULL)
        {
            g_free(unescaped);
            g_warning("Failed to resolve path for Nextcloud menu: %s", unescaped);
            return false;
        }
    }
    g_free(unescaped);
    return true;
}

static GList* tnp_provider_get_file_menu_items(ThunarxMenuProvider* menu_provider,
                                               GtkWidget* window,
                                               GList* files)
{
    char realpath_buffer[PATH_MAX];
    TnpProvider* tnp_provider = TNP_PROVIDER (menu_provider);

    // We are not supporting multiple item selection currently.
    // TODO: Add this support.
    if(files->next != NULL)
    {
        return NULL;
    }
    // items must be local
    char* uri_scheme = thunarx_file_info_get_uri_scheme (files->data);
    if (G_UNLIKELY (strcmp (uri_scheme, "file")))
    {
        g_free (uri_scheme);
        return NULL;
    }
    g_free (uri_scheme);

    // check if entry is direct descendant of a synced directory
    // i.e. check if the parent is either a synced dir or a descendant
    char * parent_uri = thunarx_file_info_get_parent_uri(files->data);
    if (!resolve_path(parent_uri, realpath_buffer) ||
        !ncsock_is_in_registered_paths(realpath_buffer)
    ) {
        g_free(parent_uri);
        return NULL;
    }
    g_free(parent_uri);

    char * uri = thunarx_file_info_get_uri(files->data);
    if (!resolve_path(uri, realpath_buffer)) {
        g_free(uri);
        return NULL;
    }
    g_free(uri);

    // append the "Share" action
    ThunarxMenuItem *nextcloud_item = thunarx_menu_item_new(
        "Tnp::nextcloud",
        ncsock_get_string("CONTEXT_MENU_TITLE", "Nextcloud"),
        NULL,
        ncsock_get_string("CONTEXT_MENU_ICON", "Nextcloud")
    );

    ThunarxMenu * submenu = thunarx_menu_new();
    thunarx_menu_item_set_menu(nextcloud_item, submenu);

    GList * subitem_list = ncsock_get_menu_items_list(realpath_buffer);
    for (GList * sub = subitem_list; sub; sub = sub->next) {
        struct nc_menu_item * sub_struct = (struct nc_menu_item *)sub->data;
        char * name = g_strdup_printf("Tnp::%s", sub_struct->action);

        ThunarxMenuItem *subitem = thunarx_menu_item_new(name, sub_struct->label, NULL, NULL);
        thunarx_menu_item_set_sensitive(subitem, !sub_struct->disable);

        if (!sub_struct->disable) {
            g_object_set_qdata_full (G_OBJECT (subitem), tnp_item_files_quark,
                                    thunarx_file_info_list_copy (files),
                                    (GDestroyNotify) thunarx_file_info_list_free);

            g_object_set_qdata_full (G_OBJECT (subitem), tnp_item_provider_quark,
                                    g_object_ref (G_OBJECT (tnp_provider)),
                                    (GDestroyNotify) g_object_unref);

            g_object_set_qdata_full (G_OBJECT (subitem), tnp_item_action_quark,
                                    g_strdup (sub_struct->action),
                                    (GDestroyNotify) g_free);

            GClosure* closure = g_cclosure_new_object(G_CALLBACK(tnp_item_do_action),
                                                    G_OBJECT (window));
            g_signal_connect_closure(G_OBJECT(subitem), "activate", closure, TRUE);
        }

        thunarx_menu_append_item(submenu, subitem);

        g_free(name);
    }
    ncsock_free_menu_items_list(subitem_list);

    GList* items = g_list_append (NULL, nextcloud_item);
    return items;
}

static void tnp_provider_class_init(TnpProviderClass* classname)
{
    GObjectClass* gobject_class;

    /* determine the "tnp-item-files", "tnp-item-folder" and "tnp-item-provider" quarks */
    tnp_item_files_quark = g_quark_from_string("tnp-item-files");
    tnp_item_provider_quark = g_quark_from_string("tnp-item-provider");
    tnp_item_action_quark = g_quark_from_string("tnp-item-action");

    gobject_class = G_OBJECT_CLASS(classname);
    gobject_class->finalize = tnp_provider_finalize;
}

static void tnp_provider_menu_provider_init(ThunarxMenuProviderIface* iface)
{
    iface->get_file_menu_items = tnp_provider_get_file_menu_items;
}

static void tnp_provider_init(TnpProvider* tnp_provider)
{
    /* connect to the socket */
    ncsock_initialize();
}

static void tnp_provider_finalize(GObject* object)
{
    TnpProvider* tnp_provider = TNP_PROVIDER(object);
    GSource* source;

    /* give up maintaince of any pending child watch */
    if (G_UNLIKELY (tnp_provider->child_watch_id != 0))
    {
        /*
        * reset the callback function to g_spawn_close_pid() so the plugin can be
        * safely unloaded and the child will still not become a zombie afterwards.
        */
        source = g_main_context_find_source_by_id (NULL, tnp_provider->child_watch_id);
        g_source_set_callback(source, (GSourceFunc) g_spawn_close_pid, NULL, NULL);
    }

    ncsock_shutdown();

    (*G_OBJECT_CLASS(tnp_provider_parent_class)->finalize)(object);
}
